# Mac Install Environment

- [Introduction](#Introduction)

# Introduction

Basic tooling and configuration for Mac environment.

# Mac Install
On the Mac, it is best to install everything from **Homebrew** for consistency and update support.

Install [Homebrew](https://brew.sh/) and use it to install gcc, CMake and Boost.