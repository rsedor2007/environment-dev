# Windows Install Environment

- [Introduction](#Introduction)

# Introduction

Basic tooling and configuration for Windows environment.

# Windows

**NOTE**
````
Veeder seems to have blocked the mingw.org pages for some reason.
The following link goes to SourceForge.net.
````

Install [mingw64](https://sourceforge.net/projects/mingw-w64/), [CMake](https://cmake.org/install/) and [Boost](https://www.boost.org/doc/libs/1_65_1/more/getting_started/windows.html).

An alternative option is to use [cygwin64](https://cygwin.com/install.html) and install the following packages:

  * boost-build
  * cmake
  * gcc-core
  * gcc-c++
  * gdb
  * libboost-devel
  * libicu-devel
  * python2
  * python3
  * subversion
  * git
  * cppcheck

### Special Windows Issues
````
Windows does not seem to be able to cleanly find Boost for instance.  Do not clutter up the CMake with a Windows issue at this point.

We should be able to get by with the following under Windows for Boost.

Just add them after the cmake call with -D. In this case it would be:

cmake -DBOOST_ROOT=C:\local\boost_1_65_1 -DBOOST_LIBRARYDIR=C:\local\boost_1_65_1\stage\lib ..

You should clear your CMake cache (delete build directory or just the CMakeCache.txt file) prior to the given command.
````