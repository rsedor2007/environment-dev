# Build Chain

- [Introduction](#Introduction)
- [Links](#Links)

# Introduction

[Code Generation](code-generation)

[Directory Structure](directory-structure.md)

[Packaging](packaging.md)

[Static Analysis](static-analysis)

[Unit Testing](unit-testing)

# Links

 * [Using clang static analyser with CMake](http://baptiste-wicht.com/posts/2014/04/install-use-clang-static-analyzer-cmake.html)
 * [Static Analysis with CDash](https://cmake.org/pipermail/cmake/2011-April/043709.html) - Includes some info about using CppCheck with CMake
 * [CppCheck Targets](https://www.openfoundry.org/svn/cms/trunk/cmake/CppcheckTargets.cmake)
 * [CMake Tips](https://samthursfield.wordpress.com/2015/10/20/some-cmake-tips/)
