# Packaging

- [Introduction](#Introduction)
- [Concepts](#Concepts)

# Introduction

Basic tooling and introduction to unit testing.
CMake has the ability to create installers for multiple platforms using a program called https://gitlab.kitware.com/cmake/community/wikis/doc/cpack/PackageGenerators[CPack].
CPack includes the ability to create Linux RPM, deb and gzip distributions of both binaries and source code.
It also includes the ability to create NSIS files for Microsoft Windows.

# Concepts


