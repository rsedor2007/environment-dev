# Configure Files Generation

- [Introduction](#Introduction)
- [Concepts](#Concepts)

# Introduction

During the call to cmake it is possible to create files that use variables from
the CMakeLists.txt and cmake cache. During CMake generation the file is copied to a
new location and any cmake variables are replaced.

The files in this tutorial are below:

```
$ tree
.
├── CMakeLists.txt
├── main.cpp
├── path.h.in
├── ver.h.in
```

  * [CMakeLists.txt](CMakeLists.txt) - Contains the CMake commands you wish to run
  * [main.cpp](main.cpp) - The source file with main
  * [path.h.in](path.h.in) - File to contain a path to the build directory
  * [ver.h.in](ver.h.in) - File to contain the version of the project

# Concepts

## Configure Files

To do variable substitution in a file you can use the `configure_file()` function
in CMake. This core arguments for this function are source file and destination file.

````
configure_file(ver.h.in ${PROJECT_BINARY_DIR}/ver.h)

configure_file(path.h.in ${PROJECT_BINARY_DIR}/path.h @ONLY)
````

The first example above, allows the variable to be defined like a CMake variables using
the `${}` syntax or an `@@` in the ver.h.in file. After generation a new file ver.h will be available
in the `PROJECT_BINARY_DIR`.

```
const char* ver = "${cf_example_VERSION}";
```

The second example, only allows variables to be defined using the `@@` syntax in the path.h.in file.
After generation a new file path.h will be available in the `PROJECT_BINARY_DIR`.

```
const char* path = "@CMAKE_SOURCE_DIR@";
```

# Building the Example

````
$ mkdir build

$ cd build/

$ cmake ..
-- The C compiler identification is GNU 
-- The CXX compiler identification is GNU 
-- Check for working C compiler: /usr/bin/cc
-- Check for working C compiler: /usr/bin/cc -- works
-- Detecting C compiler ABI info
-- Detecting C compiler ABI info - done
-- Check for working CXX compiler: /usr/bin/c++
-- Check for working CXX compiler: /usr/bin/c++ -- works
-- Detecting CXX compiler ABI info
-- Detecting CXX compiler ABI info - done
-- Configuring done
-- Generating done
-- Build files have been written to: configure-files/ode-generation/configure-files/build

$ ls
CMakeCache.txt  CMakeFiles  cmake_install.cmake  Makefile  path.h  ver.h

$ cat path.h
#ifndef __PATH_H__
#define __PATH_H__

// version variable that will be substituted by cmake
// This shows an example using the @ variable type
const char* path = "configure-files/code-generation/configure-files";

#endif

$ cat ver.h
#ifndef __VER_H__
#define __VER_H__

// version variable that will be substituted by cmake
// This shows an example using the $ variable type
const char* ver = "0.2.1";

#endif

$ make
Scanning dependencies of target cf_example
[100%] Building CXX object CMakeFiles/cf_example.dir/main.cpp.o
Linking CXX executable cf_example
[100%] Built target cf_example

$ ./cf_example
Hello Version 0.2.1!
Path is configure-files/code-generation/configure-files
````
