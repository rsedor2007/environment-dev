# Code Generation

Code generation can be useful to create source code in different languages from a common description file. 
This can reduce the amount of manual code to write and increase inter-operability.

Examples showing code generation using variables from CMake and also using some common tools.

  * [configure-file](configure-files) - Using the CMake configure_file function to inject CMake variables.
  * [Protocol Buffers](protobuf) - Using Google Protocol Buffers to generate C++ source.
