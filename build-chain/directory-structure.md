# Requirements Directory Structure

- [Directory Structure](#Directory Structure)

# Directory Structure


The files included in this example are:


```
$ tree
.
└── design-patterns-solid
    ├── CMakeLists.txt
    ├── README.md
    ├── .gitignore
    ├── /include
    ├── /src
    └── /src/solid.cpp
...

```
## Example

```
<libraryname>
    README.md
    CMakeList.txt
    CTestConfig.cmake
  include/
      <library_name>
        ...
  src/ // not required for header only libraries
    cpp files ...
    ...
  config/ // not required if no configuration files
    config files
    sh files
    bat files
    ...
  example/
    cpp and h files
    ...
  test/
    cpp and h files
    data/
      ...
  doc/
    md or adoc files
```
