# Boost Unit Testing Framework

- [Introduction](#Introduction)
- [Requirements](#Requirements)
- [Concepts](#Concepts)

# Introduction

Using [CTest](https://cmake.org/Wiki/CMake/Testing_With_CTest) you can generate
a `make test` target to run automated unit-tests. This example shows how to
find the [boost unit-test-framework](http://www.boost.org/doc/libs/1_64_0/libs/test/doc/html/utf/user-guide.html),
create tests and run them.

The files in this tutorial are below:

```
$ tree
.
├── CMakeLists.txt
├── Reverse.h
├── Reverse.cpp
├── Palindrome.h
├── Palindrome.cpp
├── unit_tests.cpp
```

  * [CMakeLists.txt](CMakeLists.txt) - Contains the CMake commands you wish to run
  * [Reverse.h](Reverse.h) / [Reverse.cpp](Reverse.cpp) - Class to reverse a string
  * [Palindrome.h](Palindrome.h) / [Palindrome.cpp](Palindrome.cpp) - Class to test if a string is a palindrome
  * [unit_test.cpp](unit_test.cpp) - Unit Tests using boost unit test framework

# Requirements

This example requires the boost libraries to be installed in a default system
location. The library in use is the boost unit-test-framework.

# Concepts

## Enabling testing

To enable testing you must include the following line in your top level CMakeLists.txt

````
enable_testing()
````

This will enable testing for the current folder and all folders below it.

## Adding a testing executable

The requirement for this step will depend on your unit-test framework. In the example
of boost, you can create binary(s) which includes all the unit tests that you want to run.

````
add_executable(unit_tests unit_tests.cpp)

target_link_libraries(unit_tests
    example_boost_unit_test
    Boost::unit_test_framework
)

target_compile_definitions(unit_tests
    PRIVATE
        BOOST_TEST_DYN_LINK
)
````

In the above code, a unit test binary is added, which links against the boost unit-test-framework
and includes a definition to tell it that we are using dynamic linking.

## Add A test

To add a test you call the [`add_test()`](https://cmake.org/cmake/help/v3.12/command/add_test.html) function.
This will create a named test which will run the supplied command.

````
add_test(test_all unit_tests)
````

In this example, we create a test called `test_all` which will run the executable
created by the `unit_tests` executable created from the call to `add_executable`

# Building the Example

````
$ mkdir build

$ cd build/

$ cmake ..
-- The C compiler identification is GNU
-- The CXX compiler identification is GNU
-- Check for working C compiler: /usr/bin/cc
-- Check for working C compiler: /usr/bin/cc -- works
-- Detecting C compiler ABI info
-- Detecting C compiler ABI info - done
-- Check for working CXX compiler: /usr/bin/c++
-- Check for working CXX compiler: /usr/bin/c++ -- works
-- Detecting CXX compiler ABI info
-- Detecting CXX compiler ABI info - done
-- Boost version: 1.64.0
-- Found the following Boost libraries:
--   unit_test_framework
-- Configuring done
-- Generating done
-- Build files have been written to: unit-testing/boost/build

$ make
Scanning dependencies of target example_boost_unit_test
[ 33%] Building CXX object CMakeFiles/example_boost_unit_test.dir/Reverse.cpp.o
[ 66%] Building CXX object CMakeFiles/example_boost_unit_test.dir/Palindrome.cpp.o
Linking CXX static library libexample_boost_unit_test.a
[ 66%] Built target example_boost_unit_test
Scanning dependencies of target unit_tests
[100%] Building CXX object CMakeFiles/unit_tests.dir/unit_tests.cpp.o
Linking CXX executable unit_tests
[100%] Built target unit_tests

$ make test
Running tests...
Test project unit-testing/boost/build
    Start 1: test_all
1/1 Test #1: test_all .........................   Passed    0.00 sec

100% tests passed, 0 tests failed out of 1

Total Test time (real) =   0.01 sec
$ make test
Running tests...
Test project unit-testing/boost/build
    Start 1: test_all
1/1 Test #1: test_all .........................   Passed    0.00 sec

100% tests passed, 0 tests failed out of 1

Total Test time (real) =   0.01 sec
````

If the code is changed and it causes the unit tests to produce an error.
Then when running the tests you will see the following output.

````
$ make test
Running tests...
Test project unit-testing/boost/build
    Start 1: test_all
1/1 Test #1: test_all .........................***Failed    0.00 sec

0% tests passed, 1 tests failed out of 1

Total Test time (real) =   0.00 sec

The following tests FAILED:
          1 - test_all (Failed)
Errors while running CTest
make: *** [test] Error 8
````
