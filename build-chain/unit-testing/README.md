# Unit Testing

- [Introduction](#Introduction)
- [Unit Testing](#Unit Testing)

# Introduction

Unit testing is a software development process in which the smallest testable parts of an
application, called units, are individually and independently scrutinized
for proper operation. This can involve taking a class, function, or algorithm
and writing test cases that can be run to verify that the unit is working correctly.

CMake includes a tool called [CTest](https://cmake.org/Wiki/CMake/Testing_With_CTest)
which allows you to enable the `make test` target to run automated tests such as unit tests.

There are many unit-testing frameworks available which can be used to help automate
and ease the development of unit tests. In these examples I show how to use
some of these frameworks and call them using the CMake testing utility CTest.

The examples here include using the following frameworks:

* [Boost Unit Test Framework](http://www.boost.org/doc/libs/1_64_0/libs/test/doc/html/utf/user-guide.html)
* [Google Test - Download](https://github.com/google/googletest)
* [Catch](https://github.com/philsquared/Catch)

Basic tooling and introduction to unit testing.

# Unit Testing

In later sessions we will be using unit tests and the [Google.Test](https://github.com/google/googletest/blob/master/googletest/docs/primer.md) framework.

[Here](https://cmake.org/cmake/help/v3.12/module/GoogleTest.html) is an introduction for using Google.Test with CMake.

Download and instructions [here](https://github.com/google/googletest).

Adding to CLion [here](https://www.jetbrains.com/help/clion/creating-google-test-run-debug-configuration-for-test.html).

The file **gtest_installer.sh** is a sample of how to download, build, and install GTest on your local machine.

If you are on a Mac or Ubuntu, add the **GTEST_DIR** and **GMOCK_DIR** to your **.bash_profile** or **.bashrc**.  On Windows, create the environment variables and add them to your path.

# Links

* [CMake, GoogleTest and CLion](https://medium.com/@TimothyHelton/c-projects-with-cmake-google-test-and-doxygen-using-clion-d508f6f4ad05)
* [Google Test and CLion](https://www.jetbrains.com/help/clion/creating-google-test-run-debug-configuration-for-test.html)
* [Appveyor](https://github.com/google/googletest/blob/master/appveyor.yml)
* [Google Test on Mac](https://gist.github.com/butuzov/e7df782c31171f9563057871d0ae444a)
* [TDD and CMake](https://youyue123.github.io/tech/2018/01/29/TDD-for-C++-in-CMake-And-GoogleTest.html)



